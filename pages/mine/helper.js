export const results = [{
	label: '流调结果',
	status:'当天有效',
	val: '绿码'
},{
	label: '健康状态',
	status:'北京市',
	val: '未见异常'
},{
	label: '核酸检测',
	status:'近3天',
	val: '阴性'
},{
	label: '疫苗接种',
	status:'北京市',
	val: '加强针已接种'
}]


export const tools = [
	{
		label: '历史申报信息',
		logo: 'history_icon.png'
	},
	{
		label: '他人健康码代查',
		logo: 'personRelation.png'
	},
	{
		label: '扫一扫',
		logo: 'answer.png'
	},{
		label: '切换至大字版',
		logo: 'switch_zoom.png'
	}
]